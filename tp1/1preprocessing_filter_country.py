import pandas as pd

from .configuration import ORGINAL_FILE, BASE_DIR

if __name__ == '__main__':

    top_n_users = 5000

    df = pd.read_csv(ORGINAL_FILE)[['userid', 'placeid', 'local_datetime',
       'category', 'country_code', 'categoryid']]

    #print(df.columns)
    df = df.query("country_code == 'BR'")

    ids = df['userid'].unique().tolist()[:top_n_users]

    df = df.query("userid in "+ str(ids))

    df.to_csv(BASE_DIR + "dataset_TIST2015_Checkins_with_Pois_8_categories_local_datetime_br_" + str(top_n_users) + "_users_2012.csv", index=False)