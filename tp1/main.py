import pandas as pd

def split_into_weekly_sets(user):

    user = user.sort_values('local_datetime')
    category_to_int = {'Travel & Transport': 0, 'Food': 1, 'Shop & Service': 2, 'Other': 3, 'College & University': 4,
                       'Arts and Entertainment': 5, 'Outdoors and Recreation': 6, 'Nightlife Spot': 7}

    df_itemset = pd.DataFrame(
        {'Travel & Transport': [], 'Food': [], 'Shop & Service': [], 'Other': [], 'College & University': [],
         'Arts and Entertainment': [], 'Outdoors and Recreation': [], 'Nightlife Spot': []})

    itemset = {'Travel & Transport': 0, 'Food': 0, 'Shop & Service': 0, 'Other': 0, 'College & University': 0,
               'Arts and Entertainment': 0, 'Outdoors and Recreation': 0, 'Nightlife Spot': 0}

    primeiro = user['local_datetime'].iloc[0]

    set_i = 0

    for i in range(1, user.shape[0]):
        atual = user['local_datetime'].iloc[i]
        if (atual - primeiro).total_seconds() > (60*60*24*7):
            #print("item", itemset)
            df_itemset = df_itemset.append(itemset, ignore_index=True)
            #print("df", df_itemset)
            itemset = {'Travel & Transport': -1, 'Food': -1, 'Shop & Service': -1, 'Other': -1, 'College & University': -1,
                       'Arts and Entertainment': -1, 'Outdoors and Recreation': -1, 'Nightlife Spot': -1}
            primeiro = atual
            category = user['category'].iloc[i]
            itemset[category] = category_to_int[category]
        else:

            category = user['category'].iloc[i]
            itemset[category] = category_to_int[category]



    return df_itemset


if __name__ == '__main__':

    df = pd.read_csv("/media/claudio/Data/backup_win_hd/Downloads/doutorado/global_foursquare/paa/tp1/dataset_TIST2015_Checkins_with_Pois_8_categories_local_datetime_br_5000_users_2012.csv")

    df['local_datetime'] = pd.to_datetime(df['local_datetime'], infer_datetime_format=True)

    df['category'] = df['category'].replace('Outdoors & Recreation', 'Outdoors and Recreation')
    df['category'] = df['category'].replace("Arts & Entertainment", "Arts and Entertainment")

    print(df['category'].unique().tolist())

    print("usuarios: ", len(df['userid'].unique().tolist()))

    df = df.groupby('userid').apply(lambda e: split_into_weekly_sets(e))

    df = df.to_numpy().tolist()

    df_new = []

    for i in range(len(df)):

        if -1 in df[i]:

            df_new.append(df[i].remove(-1))

        else:

            df_new.append(df[i])

    #print(df_new)

